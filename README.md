How I make this project scalable?
1- JWT Auth: It reduces database tables joins and make project more secure.
2- DB Index: Iterate table's rows is slow and using db index make db query fast.
3- Pagination: cursor pagination just fetch desired rows.
4- Throttle: Throttling is a process that is used to control the usage of APIs by consumers during a given period.
5- ...
