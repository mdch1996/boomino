from rest_framework import throttling


class SumAnonInvalidMethodRateThrottle(throttling.ScopedRateThrottle):

    def allow_request(self, request, view):
        if request.method == "GET":
            a = request.GET.get('a', None)
            b = request.GET.get('b', None)

            if a and b:
                try:
                    a = int(a)
                    b = int(b)
                    return True
                except Exception as e:
                    return super().allow_request(request, view)

        return super().allow_request(request, view)


class SumAnonRateThrottle(throttling.AnonRateThrottle):
    scope = 'sum_anon'


class ScopeInvalidRateThrottle(throttling.ScopedRateThrottle):

    def allow_request(self, request, view):
        print('scoped')
        if request.method.lower() in view.http_method_names:
            try:
                if request.method.lower() in ['post', 'put', 'patch'] and hasattr(view, 'serializer_class'):
                    if view.serializer_class(data=request.data).is_valid():
                        return True
                else:
                    return True
            except Exception as e:
                pass

        return super().allow_request(request, view)


class AnonInvalidRateThrottle(throttling.AnonRateThrottle):
    scope = 'invalid'

    def allow_request(self, request, view):
        if request.method.lower() in view.http_method_names:
            try:
                if request.method.lower() in ['post', 'put', 'patch'] and hasattr(view, 'serializer_class'):
                    if view.serializer_class(data=request.data).is_valid():
                        return True
                else:
                    return True
            except Exception as e:
                pass

        return super().allow_request(request, view)

