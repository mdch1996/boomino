from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.throttling import AnonRateThrottle
from rest_framework import generics
from django.db.models import Sum

from .models import Calculation
from .serializers import CalculationSerializer
from .throttlings import SumAnonRateThrottle, SumAnonInvalidMethodRateThrottle, ScopeInvalidRateThrottle
from .paginations import CustomCursorPagination


class SumAPIView(APIView):

    throttle_classes = [SumAnonRateThrottle, SumAnonInvalidMethodRateThrottle]
    serializer_class = CalculationSerializer
    permission_classes = [IsAdminUser, ]
    http_method_names = ['get']
    throttle_scope = 'invalid'

    def get(self, request):
        serializer_data = {'a': request.query_params.get('a', None),
                           'b': request.query_params.get('b', None)}
        serializer = self.serializer_class(data=serializer_data)

        if serializer.is_valid():
            calculation = serializer.save()

            return Response({'result': calculation.sum_a_and_b()}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HistoryAPIView(generics.ListAPIView):

    permission_classes = [IsAdminUser, ]
    http_method_names = ['get']
    throttle_classes = [ScopeInvalidRateThrottle, ]
    throttle_scope = 'invalid'

    serializer_class = CalculationSerializer
    pagination_class = CustomCursorPagination
    queryset = Calculation.objects.all()


class TotalAPIView(APIView):

    permission_classes = [IsAdminUser, ]
    http_method_names = ['get']
    throttle_classes = [ScopeInvalidRateThrottle, ]
    throttle_scope = 'invalid'

    def get(self, request):
        total_aggregation = Calculation.objects.all().aggregate(Sum('a'), Sum('b'))
        if total_aggregation['a__sum'] or total_aggregation['b__sum']:
            total = total_aggregation['a__sum'] + total_aggregation['b__sum']
        else:
            total = None
        return Response({'total': total}, status=status.HTTP_200_OK)



