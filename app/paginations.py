from rest_framework import pagination


class CustomCursorPagination(pagination.CursorPagination):
    cursor_query_param = 'cursor'
    ordering = ['-created_datetime', '-id']
