from rest_framework import serializers

from .models import Calculation


class CalculationSerializer(serializers.ModelSerializer):
    a = serializers.IntegerField(required=True)
    b = serializers.IntegerField(required=True)

    class Meta:
        model = Calculation
        fields = ('a', 'b')


