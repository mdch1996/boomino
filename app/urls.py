from django.urls import path
from app import views


urlpatterns = [
    path('sum/', views.SumAPIView.as_view(), name='sum'),
    path('history/', views.HistoryAPIView.as_view(), name='history'),
    path('total/', views.TotalAPIView.as_view(), name='total'),
]
