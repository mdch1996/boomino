import uuid
from django.db import models
from django.contrib.auth.models import AbstractUser


class Calculation(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    a = models.IntegerField(editable=False)
    b = models.IntegerField(editable=False)
    created_datetime = models.DateTimeField(auto_now_add=True)

    class Meta:
        index_together = [
            ("created_datetime", "id"),
        ]

    def sum_a_and_b(self):
        return self.a + self.b
